package ru.mitapp.myapp.api.repository

import ru.mitapp.myapp.api.ApiInterface
import ru.mitapp.myapp.base.BaseModel

class MainRepository(var api: ApiInterface) : BaseRepository() {

    suspend fun getPictures(): BaseModel<ArrayList<String>> {

        val response = safeApiCall(
            call = { api.getPictures().await() }
        )
        return (response as BaseModel<ArrayList<String>>)
    }
}