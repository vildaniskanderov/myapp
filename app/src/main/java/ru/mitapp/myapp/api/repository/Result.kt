package ru.mitapp.myapp.api.repository

sealed class Result<out T : Any> {

    data class Success<out T : Any>(val data: Any) : Result<T>()
    data class Error<out T : Any>(val data: Any) : Result<T>()

}