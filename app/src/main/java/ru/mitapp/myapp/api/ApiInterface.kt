package ru.mitapp.myapp.api

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface ApiInterface {

    @GET("task-m-001/list.php")
    fun getPictures(): Deferred<Response<ArrayList<String>>>

}