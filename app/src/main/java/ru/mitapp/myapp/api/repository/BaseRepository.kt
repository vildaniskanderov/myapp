package ru.mitapp.myapp.api.repository

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONObject
import retrofit2.Response
import ru.mitapp.myapp.base.BaseModel
import java.lang.reflect.Type

open class BaseRepository {

    suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>): Any? {

        val result: Result<T> = safeApiResult(call)
        var data : Any? = null

        data = when(result) {
            is Result.Success ->
                result.data
            is Result.Error -> {
                result.data
            }
        }
        return data
    }

    private suspend fun <T: Any> safeApiResult(call: suspend () -> Response<T>): Result<T> {

        val response = call.invoke()

        if (response.code() == 204){
            val baseModel : BaseModel<T> = BaseModel()
            baseModel.code = response.code()
            return Result.Success(baseModel)
        }

        if(response.isSuccessful && response.body() != null){
            val baseModel : BaseModel<T> = BaseModel()
            baseModel.data = response.body()
            baseModel.success = response.isSuccessful
            baseModel.code = response.code()
            return Result.Success(baseModel)
        }

        val errorMsg = JSONObject(response.errorBody()!!.string()).toString()
        val type : Type = object  : TypeToken<BaseModel<T>>() {}.type
        val baseModel : BaseModel<T> = Gson().fromJson(errorMsg, type)
        baseModel.code = response.code()

        return Result.Error(baseModel)
    }
}