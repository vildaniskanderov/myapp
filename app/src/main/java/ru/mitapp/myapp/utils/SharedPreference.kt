package ru.mitapp.myapp.utils

import android.content.Context
import android.content.SharedPreferences

private const val MYAPP_PREFERENCE = "myAppPrefs"


class SharedPreference(context: Context) {

    private var pref: SharedPreferences =
        context.getSharedPreferences(MYAPP_PREFERENCE, Context.MODE_PRIVATE)

    var clear: SharedPreferences.Editor = pref.edit().clear()
}