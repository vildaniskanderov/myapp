package ru.mitapp.riotokmok.utils

import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.recyclerview.widget.RecyclerView

object RecyclerAnimation {

    fun startAnimation(recyclerView: RecyclerView, animationResource : Int){
        var controller : LayoutAnimationController? = null
        controller = AnimationUtils.loadLayoutAnimation(recyclerView.context, animationResource)
        recyclerView.layoutAnimation = controller
        recyclerView.scheduleLayoutAnimation()
    }
//
//
//     fun setupIndicator(
//        indicatorContainer: LinearLayout,
//        itemCount: Int,
//        isBlack: Boolean = false
//    ) {
//        indicatorContainer.removeAllViews()
//        val imageView = arrayOfNulls<ImageView>(itemCount)
//        val layoutParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
//            ViewGroup.LayoutParams.WRAP_CONTENT,
//            ViewGroup.LayoutParams.WRAP_CONTENT
//        )
//        layoutParams.setMargins(8, 0, 8, 0)
//        for (i in imageView.indices) {
//            imageView[i] = ImageView(indicatorContainer.context)
//            imageView[i].apply {
//                if (isBlack) {
//                    this?.setImageDrawable(
//                        ContextCompat.getDrawable(
//                            indicatorContainer.context,
//                            R.drawable.list_indicator_black_inactive
//                        )
//                    )
//                } else {
//                    this?.setImageDrawable(
//                        ContextCompat.getDrawable(
//                            indicatorContainer.context,
//                            R.drawable.list_indicator_inactive
//                        )
//                    )
//                }
//
//                this?.layoutParams = layoutParams
//            }
//            indicatorContainer.addView(imageView[i])
//        }
//
//    }
//
//     fun setCurrentIndicator(
//        index: Int,
//        indicatorContainer: LinearLayout,
//        isBlack: Boolean = false
//    ) {
//        val chaildCount = indicatorContainer.childCount
//        for (i in 0 until chaildCount) {
//            val imageView = indicatorContainer[i] as ImageView
//            if (i == index) {
//                if (isBlack)
//                    imageView.setImageResource(R.drawable.list_indicator_black_active)
//                else
//                    imageView.setImageResource(R.drawable.list_indicator_black_active)
//            } else {
//                if (isBlack)
//                    imageView.setImageResource(R.drawable.list_indicator_black_inactive)
//                else
//                    imageView.setImageResource(R.drawable.list_indicator_black_inactive)
//
//            }
//        }
//    }
//
//
//    fun setupIndicatorBlack(
//        indicatorContainer: LinearLayout,
//        itemCount: Int
//    ) {
//        indicatorContainer.removeAllViews()
//        val imageView = arrayOfNulls<ImageView>(itemCount)
//        val layoutParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
//            ViewGroup.LayoutParams.WRAP_CONTENT,
//            ViewGroup.LayoutParams.WRAP_CONTENT
//        )
//        layoutParams.setMargins(8, 0, 8, 0)
//        for (i in imageView.indices) {
//            imageView[i] = ImageView(indicatorContainer.context)
//            imageView[i].apply {
//                    this?.setImageDrawable(
//                        ContextCompat.getDrawable(
//                            indicatorContainer.context,
//                            R.drawable.list_indicator_black_inactive
//                        )
//                    )
//
//                this?.layoutParams = layoutParams
//            }
//            indicatorContainer.addView(imageView[i])
//        }
//
//    }
//
//    fun setCurrentIndicatorBlack(
//        index: Int,
//        indicatorContainer: LinearLayout
//    ) {
//        val chaildCount = indicatorContainer.childCount
//        for (i in 0 until chaildCount) {
//            val imageView = indicatorContainer[i] as ImageView
//            if (i == index) {
//                    imageView.setImageResource(R.drawable.list_indicator_black_active)
//            } else {
//                    imageView.setImageResource(R.drawable.list_indicator_black_inactive)
//
//            }
//        }
//    }

}