package ru.mitapp.myapp

import android.app.Application
import android.content.Context
import ru.mitapp.myapp.api.ApiFactory
import ru.mitapp.myapp.api.repository.MainRepository
import ru.mitapp.myapp.utils.SharedPreference

class App : Application() {

    companion object {
        lateinit var sharedPreference: SharedPreference
        lateinit var context: Context
        lateinit var apiFactory: ApiFactory
        lateinit var repository: MainRepository
    }

    override fun onCreate() {
        super.onCreate()
        sharedPreference = SharedPreference(applicationContext)
        context = applicationContext

        initialisedRetrofit()
    }
    private fun initialisedRetrofit() {
        apiFactory = ApiFactory(context)
        repository = MainRepository(apiFactory.apiInterface)
    }
}