package ru.mitapp.myapp.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ru.mitapp.myapp.R
import ru.mitapp.myapp.databinding.PicturesItemBinding

class PicturesAdapter(var items: ArrayList<String>, var listener: Listener) :
    RecyclerView.Adapter<PicturesAdapter.PicturesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PicturesViewHolder {
        val binding: PicturesItemBinding? = DataBindingUtil.bind(
            LayoutInflater.from(parent.context).inflate(
                R.layout.pictures_item, parent, false
            )
        )
        return PicturesViewHolder(binding!!)
    }

    override fun onBindViewHolder(holder: PicturesViewHolder, position: Int) {
        holder.onBind(items[position], listener)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class PicturesViewHolder(var binding: PicturesItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

        fun onBind(pictures: String, listener: Listener) {
            binding.pictures = pictures
            itemView.setOnClickListener {
                listener.onPicturesClickListener(pictures, binding.picturesIv)
            }
        }
    }

    interface Listener {

        fun onPicturesClickListener(pictures: String, imageView: ImageView)
    }
}