package ru.mitapp.myapp.ui.activity

import android.content.res.Configuration
import android.widget.ImageView
import androidx.lifecycle.ViewModelProvider
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.mitapp.myapp.R
import ru.mitapp.myapp.base.BaseActivity
import ru.mitapp.myapp.databinding.ActivityMainBinding
import ru.mitapp.myapp.extension.showToast
import ru.mitapp.myapp.ui.adapter.PicturesAdapter
import ru.mitapp.myapp.ui.view_model.PicturesViewModel
import ru.mitapp.riotokmok.utils.RecyclerAnimation


class MainActivity : BaseActivity<ActivityMainBinding>(R.layout.activity_main),
    PicturesAdapter.Listener {

    private var back = false
    private var viewModel: PicturesViewModel? = null
    private lateinit var adapter: PicturesAdapter

    override fun setupView() {

        if (viewModel == null) {
            viewModel = ViewModelProvider(this)[PicturesViewModel::class.java]
            binding!!.viewModel = viewModel
            viewModel!!.pictures()
            viewModel!!.picturesdata.observe(this) {
                if (it.data != null) {
                    setupPicturesRecycler(it.data!!)
                }
            }
            refreshContent()
        }
    }

    private fun refreshContent() {
        binding!!.contentRefresh.setOnRefreshListener {
            viewModel!!.pictures()
            binding!!.contentRefresh.isRefreshing = false
        }
    }

    private fun setupPicturesRecycler(pictures: ArrayList<String>) {
        if (pictures.isNotEmpty()) {
            adapter = PicturesAdapter(pictures, this)
            binding!!.picturesRecycler.adapter = adapter
            binding!!.picturesRecycler.onFlingListener = null
            RecyclerAnimation.startAnimation(
                binding!!.picturesRecycler, R.anim.main_recycler_anim_layout
            )
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if (newConfig.orientation === Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.activity_main)
        } else if (newConfig.orientation === Configuration.ORIENTATION_LANDSCAPE) {
            setContentView(R.layout.activity_main)
        }
    }

    @DelicateCoroutinesApi
    override fun onBackPressed() {
        if (back) {
            super.onBackPressed()
        } else {
            back = true
            showToast(getString(R.string.back_toast_message))
            GlobalScope.launch {
                delay(2000L)
                back = false
            }
        }
    }

    override fun onPicturesClickListener(pictures: String, imageView: ImageView) {
        PhotoActivity.onStart(this, pictures, imageView)
    }
}