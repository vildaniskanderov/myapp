package ru.mitapp.myapp.ui.view_model

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.mitapp.myapp.App
import ru.mitapp.myapp.base.BaseModel
import ru.mitapp.myapp.base.BaseViewModel
import java.lang.Exception

class PicturesViewModel : BaseViewModel() {

    var isLoad = ObservableField(false)

    val picturesdata: MutableLiveData<BaseModel<ArrayList<String>>> by lazy {
        MutableLiveData<BaseModel<ArrayList<String>>>()
    }

    fun pictures() {
        scope.launch {
            try {
                isLoad.set(false)
                val response = App.repository.getPictures()
                setPicturesData(response)
                isLoad.set(false)
            } catch (e: Exception) {
                e.stackTrace
                isLoad.set(false)
            }
        }
    }

    private suspend fun setPicturesData(response: BaseModel<ArrayList<String>>): LiveData<BaseModel<ArrayList<String>>> {
        withContext(Dispatchers.Main) {
            picturesdata.value = response
        }
        return picturesdata
    }
}