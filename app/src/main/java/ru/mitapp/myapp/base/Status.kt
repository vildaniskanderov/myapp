package ru.mitapp.riotokmok.base

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}