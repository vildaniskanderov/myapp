package ru.mitapp.myapp.base

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

abstract class BaseActivity<DataBinding : ViewDataBinding>(private val layoutId: Int) :
    AppCompatActivity(){

    var binding: DataBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (binding == null)
        binding = DataBindingUtil.setContentView(this, layoutId)

        setupView()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            onBackPressed()
        return true
    }

    abstract fun setupView()

}