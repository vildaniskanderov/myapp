package ru.mitapp.myapp.base

data class BaseModel<T>(
    var data : T? = null,
    var success : Boolean? = false,
    var message : String? = null,
    var code : Int? = null
)