package ru.mitapp.riotokmok.base


import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import ru.mitapp.myapp.R


@BindingAdapter("imageUrl")
fun loadImage(imageView: ImageView, url: String?) {
    Glide.with(imageView.context).load(url).placeholder(R.drawable.ic_placeholder).into(imageView)
}

@BindingAdapter("srcDrawable")
fun setSrcDrawable(imageView: ImageView, drawable : Drawable) {
    imageView.setImageDrawable(drawable)
}